## v5 build
![photo-0](/build-images/v5/plate-switch.jpeg "pom plate and 205g0 lightly lubed filmed azure dragon switch")

![photo-1](/build-images/v5/another-angle.jpeg "plate switch installed another angle looking")

![photo-2](/build-images/v5/assemble.jpeg "assembled without keycaps")

![photo-3](/build-images/v5/fully-built.jpeg "fully built (old pcb from v2 btw)")

![photo-4](/build-images/v5/bottom-side.jpeg "looking at bottom rubber feet")

![photo-5](/build-images/v5/ixpe-switch-foam.jpeg "looking at 0.5mm ixpe switch foam")


![v5-show-case](/build-images/v5/quick-tapping-video.mp4)


## v2 build
![photo-0](/build-images/v2/0.jpeg "test to see rgb with pom plate")

![photo-1](/build-images/v2/1.jpeg "check center align of usb port hole")

![photo-2](/build-images/v2/2.jpeg "test plate switch hole position all correct")

![photo-3](/build-images/v2/3.jpeg "pcb plug in test")

![photo-4](/build-images/v2/4.jpeg "pcb plug in test")

![photo-5](/build-images/v2/5.jpeg "looking at case")

![photo-6](/build-images/v2/6.jpeg "looking at case")

![photo-7](/build-images/v2/7.jpeg "looking at case")

![photo-8](/build-images/v2/8.jpeg "looking at case")

![photo-9](/build-images/v2/9.jpeg "looking at case")

![photo-10](/build-images/v2/10.jpeg "looking at case")

![photo-11](/build-images/v2/11.jpeg "looking at case")

![photo-12](/build-images/v2/12.jpeg "looking at gasket strip")

![photo-13](/build-images/v2/13.jpeg "very precise laser cut gasket strip")

![photo-14](/build-images/v2/14.jpeg "full build")

![photo-15](/build-images/v2/15.jpeg "after installed all switches")

![photo-16](/build-images/v2/18.jpeg "case foam that stick to back of pcb")

![photo-16](/build-images/v2/16.jpeg "after case assembled and switches installed")

![photo-17](/build-images/v2/17.jpeg "full build powered on")
