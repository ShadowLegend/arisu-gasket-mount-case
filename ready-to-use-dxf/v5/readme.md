
### files to be given to corresponding laser cutting service provider
- `v5-case.dxf`, case and plate both in this file
- `v5-foams.dxf`, poron gasket and plate foam in here, do not have switch foam and case foam but I give them to corresponding laser cutting service that knows keyboard stuff and they able to edit the file to turn it into switch foam(pe foam) and case foam(foam that stick to back of pcb or case foam, the seller I went to require to take pic of back of my pcb and plate file and able to make it quite precise(99% precise), see build image for this)
- `v5-rubber-feet.dxf`, also custom rubber feet(about freaking 15$ just to custom make this), I used silicone for this, u can try eva or poron since it is should be way cheaper, but I find silicone rubber more effective to prevent sliding and underglow light can went through as well and result are up to expectation(have quick video to show this)


### screws and standoff

- hex standoff 2*12 = 8(stainless steel)
- M2*12 screw = 8 with riser, 16 without riser
- M2*8 screw = 2
- M2*6 screw = 4
