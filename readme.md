## alice arisu gasket mount case

## NOTE: Please build this at your own risk, I will provide sample of the build after I got the full case

## Credit
- [av3 keyboard](https://iv-works.com/av3/) - design inspired by av3 keyboard
- [FateNozomi/arisu-case](https://github.com/FateNozomi/arisu-case) - took the case file as base for design

## description
designed inspired by av3 work keyboard, but since waiting group by takes time and do not know for sure if it is going to be back and have no budget, so I have draw the case myself and take it to local acrylic shop to cut it for very cheap price.

the objective here is to make it gasket mount to have that nice soft typing feel and raindrop sounds, which the case did achieved its objective.

since pcb also difficult to find at the time of writing and althou there are open source pcb file for it but order a custom pcb made here is also expensive, but fortunately I have found one from Aliexpress, which is Wings hotswap rgb pcb from YMDK for 60$, there are also 30$ one for solder version.

## case files screenshots
![case-v5](/case-v5.png "v5 case file")

![gasket-strip-v5](/gasket-strip-v5.png "gasket strips v5")

![rubber-feet-v5](/rubber-feet-v5.png "rubber feet v5")

![foams](/foams.png "other optional foams but recommend to use both plate and case foam for fuller keyboard sound experience")

NOTE: build images located within `/build-images`
NOTE: because of covid situation, shipping got delayed and stuff so only part of the staff came so maybe at the time u are looking in the directory the build may not be completed yet

## screws and standoffs
- hex standoff 2*12 = 8 (brass for v2, stainless steel for v5 due to different material have different sizing, just realized this problem when designing v5 when I want to use stainless steel instead for asethetic)
- M2*12 screw = 8 with riser, 16 without riser
- M2*8 screw = 2
- M2*6 screw = 4

## v5 build
![v5-fully-built](/v5-fully-built.jpeg "v5 fully built")
![case-v5-top](/v5-case-top.jpeg "v5 case top")
![case-v5-bottom](/v5-case-bottom.jpeg "v5 case bottom")
![case-v5-side](/v5-case-side.jpeg "v5 case side")
![case-v5-side-to-see-rubber-feet-touch](/v5-case-side-another.jpeg "v5 case side, rubber feet does touch the table as expected, will post video to show this after full build")
![case-v5-middle](/v5-case-middle.jpeg "v5 case middle")

## full v2 build
![case-v2-build](/build-images/v2/17.jpeg "v2 case full build")

## spec
- 0.5mm ixpe switch foam
- 3.5mm poron plate foam
- 3.2mm poron bottom pcb foam laser cut and stick to back of pcb with led holes
- azure dragon switches lightly lubed with 205g0 and film with 0.125mm tx film(stem, avoid leg, rails, bottom housing, top housing and donut dip spring)
- clone gmk classic blue(double shot abs)
- ymdk wings hotswap
- pom plate
- tx stab wire and stem hole lubed with xht-bdz and stem with 205g0(no wire balancing or disassemble just lube over the pre-assembled stab)

## v5 typing sound
### record using samsung s9+, with 4mm deskmat, spec as above written in spec section

![v5-show-case](/build-images/v5/quick-tapping-video.mp4)

![v5-typing-sound](/v5-typing-sound-test-with-ixpe-switch-foam.wav)
